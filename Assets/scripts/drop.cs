﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drop : MonoBehaviour
{
    public GameObject upplerbound;
    public GameObject lowerbound;
    public GameObject leftbound;
    public GameObject rightbound;
    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = new Vector3(Random.Range(leftbound.transform.position.x +0.5f,rightbound.transform.position.x-0.5f), upplerbound.transform.position.y, this.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Vector3.down*Time.deltaTime*3);
        if(this.transform.position.y < lowerbound.transform.position.y + 0.5)
        {
            Destroy(this.gameObject);
        }
    }
}
