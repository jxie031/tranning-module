﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour
{
    void restart()
    {
        endgame_canvas.gameObject.SetActive(false);
        health = 5;
        for (int i = 0; i < health; i++)
        {
            Health[i].gameObject.SetActive(true);
            score = 0;
            timespace = 2f;
        }
    }
    public Image[] Health = new Image[5];
    public Canvas endgame_canvas;
    public Material player_material;
    public Image pauseUI;
    public Button restart_but;
    [SerializeField] private GameObject pausePanel;
    public Text Score;
    public Text EndgameScore;
    public int score = 0;
    public static int health = 5;
    private int rand = 2;
    public GameObject scoreobj;
    public GameObject badobj;
    private float timeleft = 0.5f;
    private float timespace = 2f;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "score")//if player hit the score obj, the score value increase
        {
            score++;
            player_material.color = Random.ColorHSV();
        }
        else if(other.gameObject.tag == "bad")// player hit the bad obj, the health value decrease
        {
            if (health > 1)
            {
                Health[--health].gameObject.SetActive(false);
            }
            else
            {
                //endgame canvase shows up
                endgame_canvas.gameObject.SetActive(true);
                //gamepause
                pauseUI.gameObject.SetActive(true);
                Time.timeScale = 0;
                pausePanel.SetActive(true);
                EndgameScore.text = score.ToString();

            }
        }
        Destroy(other.gameObject);


    }
    CharacterController characterController;//using unity characterControler component
    public float speed = 15.0f;//player moving speed
    private Vector3 moveDirection = Vector3.zero;// initialize the move direction
    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        pausePanel.SetActive(false);
        restart_but.onClick.AddListener(restart);
    }

    // Update is called once per frame
    void Update()
    {
        timeleft -= Time.deltaTime;
        if(timeleft < 0) {
            if (rand == 2)// if the random variable is 2, we drop the score obj
            {
                GameObject dup = Instantiate(scoreobj);

                dup.SetActive(true);
            }
            else if (rand == 1)// if the random variable is 1, we drop the bad obj
            {
                GameObject dup2 = Instantiate(badobj);

                dup2.SetActive(true);
            }if (timespace > 0.5f)
            {
                timeleft = timespace;
                timespace -= 0.05f;// decrease the time space until it reach 0.5f (most dangerous mode)
            }
            else
            {
                timeleft = timespace;
            }
        }
        rand = Random.Range(1, 3);// use random variable
        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, 0.0f);//move directions are only left and right
        moveDirection *= speed;//Player moving speed
        characterController.Move(moveDirection * Time.deltaTime);
        Score.text = "Score: " + score.ToString();

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(pausePanel.activeInHierarchy == false)
            {
                pauseUI.gameObject.SetActive(true);
                Time.timeScale = 0;
                pausePanel.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                pausePanel.SetActive(false);
                pauseUI.gameObject.SetActive(false);
            }
        }
        if(endgame_canvas.gameObject.activeInHierarchy == true)
        {

        }
    }
}
